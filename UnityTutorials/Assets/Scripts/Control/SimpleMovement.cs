﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A very simply movement script that accellerates the character to the left and right. Upwards movement is done
/// with a jump.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class SimpleMovement : MonoBehaviour
{
    /// <summary>
    /// The maximum speed of the movement horizontally.
    /// </summary>
    [SerializeField]
    private float _maxSpeed;

    /// <summary>
    /// The acceleration that is used in the horizontal.
    /// </summary>
    [SerializeField]
    private float _acceleration;

    /// <summary>
    /// The deceleration applied whenever forces are not being applied.
    /// </summary>
    [SerializeField]
    private float _horizontalDrag;

    /// <summary>
    /// The force exerted whenever the character is meant to go upward.
    /// </summary>
    [SerializeField]
    private float _upwardsForce;

    private Vector2 _movementInfo;
    private Rigidbody2D _body;

    private void Awake()
    {
        _body = GetComponent<Rigidbody2D>();
    }

    public void Move(float direction, bool upwards)
    {
        _movementInfo.x = direction;
        _movementInfo.y = upwards ? 1 : -1;
    }

    private void FixedUpdate()
    {
        var vel = _body.velocity;
        if(_movementInfo.x > 0 || _movementInfo.x < 0) {
            if(Mathf.Sign(vel.x) == Mathf.Sign(_movementInfo.x)) {
                vel.x += _acceleration * _movementInfo.x * Time.fixedDeltaTime;
            }
            else {
                vel.x += _acceleration * 2 * _movementInfo.x * Time.fixedDeltaTime;
            }
            
            if(vel.x > _maxSpeed) {
                vel.x = _maxSpeed;
            }
            else if(vel.x < -_maxSpeed) {
                vel.x = -_maxSpeed;
            }
        }
        else {
            vel.x = vel.x * (1 - Time.fixedDeltaTime * _horizontalDrag);
        }

        _body.velocity = vel;
    }

    private void LateUpdate()
    {
        //Do this here because it will be more reliable since the input is simply applied at once.
        if(_movementInfo.y > 0) {
            _body.velocity = new Vector2(_body.velocity.x, _upwardsForce);
            _movementInfo.y = 0;
        }
    }
}
