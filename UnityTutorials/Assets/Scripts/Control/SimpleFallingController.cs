﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A simple controller that facilitates a falling object.
/// </summary>
[RequireComponent(typeof(SimpleMovement))]
public class SimpleFallingController : MonoBehaviour
{
    //Shows up in editor and can be changed by outside classes.
    public PlayerInputHandler inputHandler;

    //Shows up in the editor, but cannot be changed by outside classes.
    [SerializeField]
    private float _timeBetweenJumps;

    //Does not show up in the editor. These are local only to the class.
    private SimpleMovement _movement;
    private float _timeSinceLastJump;

    //Magic
    private void Awake()
    {
        //Pretend that our jump is ready to go from the beginning.
        _timeSinceLastJump = _timeBetweenJumps;
    }

    //Magic
    private void Start()
    {
        _movement = GetComponent<SimpleMovement>();
    }

    //Magic
    private void Update()
    {
        //Consolidate the jump check with the button plus the delay.
        var direction = 0f;
        if(!Mathf.Approximately(inputHandler.Direction, 0)) {
            direction = inputHandler.Direction;
        }

        var doJump = CheckJump();
        _movement.Move(direction, doJump);
    }

    //Magic
    private void LateUpdate()
    {
        if (_timeSinceLastJump < _timeBetweenJumps) {
            _timeSinceLastJump += Time.deltaTime;
        }
    }

    //Not magic, but public so its available to outside classes.
    public float GetPercentRemainingForJump()
    {
        return _timeSinceLastJump / _timeBetweenJumps;
    }

    //Not magic, but private so it's only available inside of the class.
    private bool CheckJump()
    {
        var doJump = inputHandler.Upwards && _timeSinceLastJump >= _timeBetweenJumps;
        if(doJump) {
            _timeSinceLastJump = 0;
        }

        return doJump;
    }
}
