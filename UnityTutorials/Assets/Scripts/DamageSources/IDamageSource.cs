﻿public interface IDamageSource
{
    int DamageAmount { get; }
}