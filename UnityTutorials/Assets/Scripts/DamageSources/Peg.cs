﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peg : MonoBehaviour, IDamageSource
{
    public int DamageAmount => 1;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        var health = collision.collider.GetComponent<PlayerHealth>();
        if(health != null) {
            health.TakeHit(this);
        }
    }
}
