﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    private PlayerHealth _playerHealth;

    private void Start()
    {
        _playerHealth = PlayerHealth.Instance;
        _playerHealth.OnHealthChanged.AddListener(HandleHealthChanged);
    }

    private void OnDestroy()
    {
        if(_playerHealth != null) {
            _playerHealth.OnHealthChanged.RemoveListener(HandleHealthChanged);
        }
    }

    private void HandleHealthChanged(PlayerHealth player)
    {
        if(_playerHealth.RemainingHealthPoints <= 0) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
