﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{
    [SerializeField]
    private GameObject _lifeObject;

    [SerializeField]
    private RectTransform _lifeContainer;

    private GameObject[] _lifeObjects;
    private PlayerHealth _health;

    private void Start()
    {
        _health = PlayerHealth.Instance;
        _lifeObjects = new GameObject[_health.RemainingHealthPoints];
        for(var i = 0; i < _health.RemainingHealthPoints; ++i) {
            _lifeObjects[i] = Instantiate(_lifeObject, _lifeContainer);
        }

        _health.OnHealthChanged.AddListener(HandleHealthChanged);
    }

    private void OnDestroy()
    {
        if(_health != null) {
            _health.OnHealthChanged.RemoveListener(HandleHealthChanged);
        }
    }

    private void HandleHealthChanged(PlayerHealth health)
    {
        var remainingHealth = health.RemainingHealthPoints;
        if(remainingHealth < 0) {
            foreach(var obj in _lifeObjects) {
                obj.SetActive(false);
            }
        }
        else {
            _lifeObjects[remainingHealth].SetActive(false);
        }
    }
}
