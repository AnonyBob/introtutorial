﻿using UnityEngine;

public class RigidUIFollower : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    [SerializeField]
    private Camera _targetCamera;

    [SerializeField]
    private Vector2 _offset;

    private RectTransform _canvasTransform;
    private Camera _canvasCamera;

    private void Start()
    {
        var canvas = GetComponentInParent<Canvas>();
        _canvasCamera = canvas.worldCamera;
        _canvasTransform = canvas.transform as RectTransform;
    }

    private void FixedUpdate()
    {
        var screenPos = _targetCamera.WorldToScreenPoint(_target.position);
        screenPos.z = 0;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasTransform, screenPos, _canvasCamera, out var localPoint);
        transform.localPosition = localPoint + _offset;
    }
}