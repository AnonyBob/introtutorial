﻿using UnityEngine;
using UnityEngine.UI;

public class TimeRemainingInJump : MonoBehaviour
{
    [SerializeField]
    private Image _fillImage;

    [SerializeField]
    private SimpleFallingController _fallingController;

    private void LateUpdate()
    {
        if(_fallingController == null) {
            return;
        }

        _fillImage.fillAmount = _fallingController.GetPercentRemainingForJump();
    }
}
