﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simply captures the input values of the player.
/// </summary>
public class PlayerInputHandler : MonoBehaviour
{
    public float Direction => Input.GetAxis("Horizontal");
    public bool Upwards => Input.GetButtonDown("Fire1");
}
