﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(PlayerHealth))]
public class DamageFlash : MonoBehaviour
{
    [SerializeField]
    private Color _colorFlash;

    [SerializeField]
    private float _flashDuration;

    [SerializeField]
    private AnimationCurve _flashCurve;

    private SpriteRenderer _renderer;
    private PlayerHealth _health;

    private int _previousHealthPoints;
    private MaterialPropertyBlock _block;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _block = new MaterialPropertyBlock();
        _renderer.GetPropertyBlock(_block);

        _health = GetComponent<PlayerHealth>();
        _health.OnHealthChanged.AddListener(HandleHealthChange);
    }

    private void Start()
    {
        _previousHealthPoints = _health.RemainingHealthPoints;
    }

    private void HandleHealthChange(PlayerHealth playerHealth)
    {
        if(playerHealth.RemainingHealthPoints < _previousHealthPoints) {
            StopAllCoroutines();
            StartCoroutine(DoFlash());
        }
    }

    private IEnumerator DoFlash()
    {
        var time = 0f;
        var currentColor = _colorFlash;
        currentColor.a = 0;

        while(time <_flashDuration) {

            currentColor.a = _flashCurve.Evaluate(time / _flashDuration);
            _block.SetColor("_Highlight", currentColor);
            _renderer.SetPropertyBlock(_block);

            yield return null;
            time += Time.deltaTime;
        }

        currentColor.a = 0;
        _block.SetColor("_Highlight", currentColor);
        _renderer.SetPropertyBlock(_block);
    }
}