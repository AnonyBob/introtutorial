﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// The total health of the player.
/// </summary>
public class PlayerHealth : MonoBehaviour
{
    public static PlayerHealth Instance
    {
        get;
        private set;
    }

    [SerializeField]
    private int _startingHealthPoints;

    [SerializeField]
    private HealthChangedEvent _onHealthChanged;
    public HealthChangedEvent OnHealthChanged => _onHealthChanged;

    private int _remainingHealthPoints;
    public int RemainingHealthPoints
    {
        get => _remainingHealthPoints;
        private set
        {
            if(_remainingHealthPoints != value) {
                _remainingHealthPoints = value;
                _onHealthChanged.Invoke(this);
            }
        }
    }

    //To ensure that the sources of damage only hurt the player once.
    private readonly HashSet<IDamageSource> _hasAlreadyHit = new HashSet<IDamageSource>();

    private void Awake()
    {
        if(Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
        }

        Restart();
    }

    public void TakeHit(IDamageSource source)
    {
        if(_hasAlreadyHit.Add(source)) {
            RemainingHealthPoints--;
        }
    }

    public void Restart()
    {
        RemainingHealthPoints = _startingHealthPoints;
        _hasAlreadyHit.Clear();
    }

    [System.Serializable]
    public class HealthChangedEvent : UnityEvent<PlayerHealth> { }
}
