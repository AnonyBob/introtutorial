﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidCameraFollower : MonoBehaviour
{
    public Transform target;

    public Vector3 offset;

    // Update is called once per frame
    void LateUpdate()
    {
        var position = target.position + offset;
        position.x = transform.position.x;
        this.transform.position = position;
    }
}
